import './App.css';
import Calculator from "./containers/Calculator/Calculator";

function App() {
  return (
    <Calculator />
  );
}

export default App;
