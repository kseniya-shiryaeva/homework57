import React from 'react';
import './CalcTypeChanger.css';

const CalcTypeChanger = ({calcTypeEqually,changeCalcType}) => {
    return (
        <div className="CalcTypeChanger">
            <h4>Сумма заказа считается:</h4>
            <label>
                <input type="radio" name="calc_type" value={true} checked={calcTypeEqually} onChange={() => changeCalcType(true)} />
                Поровну между всеми участниками
            </label>
            <label>
                <input type="radio" name="calc_type" value={false} checked={!calcTypeEqually} onChange={() => changeCalcType(false)} />
                Каждому индивидуально
            </label>
        </div>
    );
};

export default CalcTypeChanger;