import React from 'react';
import './EquallyCalcForm.css';

const EquallyCalcForm = ({calcSum, showResult, total, personSum, equallyCalcData, onInputChange}) => {
    let result = '';

    if(showResult) {
        result = <div className="Result">
            <p>Общая сумма: {total}</p>
            <p>Количество человек: {equallyCalcData.people}</p>
            <p>Каждый платит по: {personSum}</p>
        </div>;
    }

    return (
        <div className="EquallyCalcForm">
            <form onSubmit={calcSum}>
                <label>
                    <span className="col">Человек:</span>
                    <input
                        type="number"
                        name="people"
                        value={equallyCalcData.people}
                        onChange={onInputChange}
                    />
                </label>
                <label>
                    <span className="col">Сумма заказа:</span>
                    <input
                        type="number"
                        name="order_sum"
                        value={equallyCalcData.order_sum}
                        onChange={onInputChange}
                    />
                </label>
                <label>
                    <span className="col">Процент чаевых:</span>
                    <input
                        type="number"
                        name="tips_perc"
                        value={equallyCalcData.tips_perc}
                        onChange={onInputChange}
                    />
                </label>
                <label>
                    <span className="col">Доставка:</span>
                    <input
                        type="number"
                        name="delivery"
                        value={equallyCalcData.delivery}
                        onChange={onInputChange}
                    />
                </label>
                <div className="controlBlock">
                    <button type="submit">Рассчитать</button>
                </div>
            </form>
            {result}
        </div>
    );
};

export default EquallyCalcForm;