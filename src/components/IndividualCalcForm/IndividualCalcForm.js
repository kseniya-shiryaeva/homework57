import React from 'react';
import Urn from '../../assets/trash-alt-solid.svg';
import './IndividualCalcForm.css';

const IndividualCalcForm = ({calcSum, showResult, people, total, onChangeItem, addItem, equallyCalcData, onInputChange, paySum}) => {
    let result = '';

    if(showResult) {
        result = <div className="Result">
            <p>Общая сумма: {total}</p>
            {paySum.map(person => {
                return <div key={person.key}>{person.name}: {person.sum} KGS</div>
            })}
        </div>;
    }

    const peopleFields = people.map(person => {
        return <div className="Person" key={person.key}>
            <input type="text" name="name" value={person.name} onChange={e => onChangeItem(e)} id={person.key} />
            <input type="number" name="sum" value={person.sum} onChange={e => onChangeItem(e)} id={person.key} /> сом
            <button type="button" className="removeButton">
                <img src={Urn} alt="Remove"/>
            </button>

        </div>;
    });

    return (
        <div className="IndividualCalcForm">
            <form onSubmit={calcSum}>
                {peopleFields}
                <div><button type="button" onClick={addItem} className="addButton">+</button></div>
                <div className="parameters">
                    <label>
                        <span>Процент чаевых:</span>
                        <input
                            type="number"
                            name="tips_perc"
                            value={equallyCalcData.tips_perc}
                            onChange={onInputChange}
                        /> %
                    </label>
                    <label>
                       <span>Доставка:</span>
                        <input
                            type="number"
                            name="delivery"
                            value={equallyCalcData.delivery}
                            onChange={onInputChange}
                        /> сом
                    </label>
                </div>

                <div className="submitButton"><button type="submit">Рассчитать</button></div>
            </form>
            {result}
        </div>
    );
};

export default IndividualCalcForm;