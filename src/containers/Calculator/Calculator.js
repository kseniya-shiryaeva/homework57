import React, {useEffect, useState} from 'react';
import CalcTypeChanger from "../../components/CalcTypeChanger/CalcTypeChanger";
import EquallyCalcForm from "../../components/EquallyCalcForm/EquallyCalcForm";
import './Calculator.css';
import IndividualCalcForm from "../../components/IndividualCalcForm/IndividualCalcForm";
import {nanoid} from "nanoid";

const Calculator = () => {
    const [calcTypeEqually, setCalcTypeEqually] = useState(true);

    const [equallyCalcData, setEquallyCalcData] = useState({
        people: 0,
        order_sum: 0,
        tips_perc: 0,
        delivery: 0
    });

    const [individualCalcData, setIndividualCalcData] = useState([
        {
            key: nanoid(),
            name: '',
            sum: 0
        }
    ]);

    const [total, setTotal] = useState(0);
    const [personSum, setPersonSum] = useState(0);
    const [paySum, setPaySum] = useState([]);

    const [showResult, setShowResult] = useState(false);

    const changeCalcType = type => {
        setCalcTypeEqually(type);
    }

    const onInputChange = e => {
        const {value, name} = e.target;

        let equallyCalcDataCopy = ({
            ...equallyCalcData,
            [name]: parseInt(value)
        });

        setEquallyCalcData(equallyCalcDataCopy);

        if (calcTypeEqually) {
            const total = equallyCalcDataCopy.order_sum + (equallyCalcDataCopy.order_sum / 100 * equallyCalcDataCopy.tips_perc) + equallyCalcDataCopy.delivery;
            let person_sum = 0;
            if (equallyCalcDataCopy.people > 0) {
                person_sum = Math.ceil(total / equallyCalcDataCopy.people);
            }

            setTotal(total);
            setPersonSum(person_sum);
        } else {
            let totalSum = 0;
            let total = individualCalcData.reduce(function (accumulator, item) {
                return accumulator + parseInt(item.sum);
            }, totalSum)

            total = total + (total / 100 * equallyCalcDataCopy.tips_perc) + equallyCalcDataCopy.delivery;
            setTotal(total);
            setPaySum(individualCalcData.map(item => {
                const sum = parseInt(item.sum) + item.sum / 100 * equallyCalcDataCopy.tips_perc + Math.ceil(equallyCalcDataCopy.delivery / individualCalcData.length);
                return {key: item.key, name: item.name, sum: sum};
            }))
        }
    }

    const showSum = e => {
        e.preventDefault();
        setShowResult(true);
    }

    const onChangeItem = e => {
        const {value, name, id} = e.target;

        const setIndividualCalcDataCopy = [...individualCalcData];

        setIndividualCalcData(setIndividualCalcDataCopy.map(item => {
            if (item.key === id) {
                item[name] = value;
            }
            return item;
        }));

    }

    const addItem = () => {
        setIndividualCalcData(prev => ([...prev, {
            key: nanoid(),
            name: '',
            sum: 0
        }]));
    }

    let form = <EquallyCalcForm
        showResult={showResult}
        equallyCalcData={equallyCalcData}
        total={total}
        personSum={personSum}
        calcSum={showSum}
        onInputChange={onInputChange}
    />;

    if (!calcTypeEqually) {
        form = <IndividualCalcForm
            people={individualCalcData}
            calcSum={showSum}
            total={total}
            showResult={showResult}
            onChangeItem={e => onChangeItem(e)}
            addItem={addItem}
            equallyCalcData={equallyCalcData}
            onInputChange={onInputChange}
            paySum={paySum}
        />;
    }

    return (
        <div className="Calculator">
            <CalcTypeChanger calcTypeEqually={calcTypeEqually} changeCalcType={changeCalcType} />
            {form}
        </div>
    );
};

export default Calculator;